import {
  default as React,
  Component,
} from "react";

import {
  withGoogleMap,
  GoogleMap,
  Marker,
} from "react-google-maps";

const GettingStartedGoogleMap = withGoogleMap(props => (
  //Draw map center around the United States
  <GoogleMap
    ref={props.onMapLoad}
    defaultZoom={3}
    defaultCenter={{ lat: 42, lng: -97 }}
    onClick={props.onMapClick}
  >
    {props.markers.map(marker => (
      <Marker
        {...marker}
        onRightClick={() => props.onMarkerRightClick(marker)}
      />
    ))}
  </GoogleMap>
));

export default class GettingStartedExample extends Component {
  constructor(props) {
    super(props);
    this.state =  {
      markers: [],
    };

    this.handleMapLoad = this.handleMapLoad.bind(this);
    this.handleMapClick = this.handleMapClick.bind(this);
    this.handleMarkerRightClick = this.handleMarkerRightClick.bind(this);
  }

  handleMapLoad(map) {
    this._mapComponent = map;
    if (map) {
      console.log(map.getZoom());
    }
  }

  /*
   * This is called when you click on the map.
   */
  handleMapClick(event) {

    if (this.state.markers.length === 2) {
      alert('right click a location to delete it');
      return;
    }

    const nextMarkers = [
      ...this.state.markers,
      {
        position: event.latLng,
        defaultAnimation: 2,
        key: Date.now(),
      },
    ];

    this.setState({
      markers: nextMarkers,
    });

    if(nextMarkers.length === 2) { //After two markers have been placed, get information about them.
      let service = new google.maps.DistanceMatrixService();

      let origin = new google.maps.LatLng(this.state.markers[0].position.lat(), this.state.markers[0].position.lng());
      let destination = new google.maps.LatLng(this.state.markers[1].position.lat(), this.state.markers[1].position.lng());

      service.getDistanceMatrix(
        {
          origins: [origin],
          destinations: [destination],
          travelMode: 'DRIVING',
          unitSystem: google.maps.UnitSystem.METRIC,
          avoidHighways: false,
          avoidTolls: false,
        }, matrixDistanceAPICallback.bind(this));

      //Callback that is executed after a request completes
      function matrixDistanceAPICallback(response, status) {
        if (status == 'OK') {

          //Start parsing any information we can get
          var origins = response.originAddresses;
          var destinations = response.destinationAddresses;

          for (var i = 0; i < origins.length; i++) {
            var results = response.rows[i].elements;
            for (var j = 0; j < results.length; j++) {
              let element = results[j];
              if(element.status !== "ZERO_RESULTS") {
                let distance = element.distance.text;
                let duration = element.duration.text;
                let from = origins[i];
                let to = destinations[j];
                const info = {
                  distance,
                  from,
                  to,
                  duration,
                };
                this.setState({
                  info,
                })
              } else {
                alert("No data available, make sure you pick locations that are drivable between one another");
              }
            }
          }
        }
      }
    }

  }

  //User has right clicked
  handleMarkerRightClick(targetMarker) {
    const nextMarkers = this.state.markers.filter(marker => marker !== targetMarker);
    this.setState({
      markers: nextMarkers,
    });
  }

  //If available display information between two locations.
  renderInfo() {
    if (this.state.info) {
      return (
        <div>
          <br />
          Travel route is between <strong>{this.state.info.from}</strong> and <strong>{this.state.info.to}</strong>
          <br />
          <label>Distance: </label> {this.state.info.distance}
          <br />
          <label>Driving Time: </label> {this.state.info.duration}
        </div>
      )
    }
  }

  render() {
    return (
      <div style={{height: `100%`}}>
        <GettingStartedGoogleMap
          containerElement={
            <div style={{ height: 500 }} />
          }
          mapElement={
            <div style={{ height: 500 }} />
          }
          onMapLoad={this.handleMapLoad}
          onMapClick={this.handleMapClick}
          markers={this.state.markers}
          onMarkerRightClick={this.handleMarkerRightClick}
        />
        {this.renderInfo()}
      </div>
    );
  }
}
