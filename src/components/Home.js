import React from "react";
import MainMap from './MainMap';

// Home page component
export default class Home extends React.Component {
  // render
  render() {
    return (
      <div className="page-home">
        <center>
          <h2>Distance and Travel Application</h2>
          <h4>Click on two locations to see the distance/travel information between them</h4>
        </center>

        <MainMap />

      </div>
    );
  }
}
