This application takes two clicks from a Google Map and displays driving travel information between the two locations.

To run the application you must do this:

npm install

Then to run as a dev:

npm start

More scripts can be found in the package.json file.

I started with the "redux-minimal" boilerplate project (http://redux-minimal.js.org/), although I didn't use Redux at all.

To use the project yourself, please make sure you change the API key for the google map API under public/index.html.
This project uses the Google Distance Matrix API, so also please make sure you have that Google API enabled.


To change the API key change the script that looks like this:
<script src="https://maps.googleapis.com/maps/api/js?key=API_KEY"></script>

and replace "API_KEY" with your key.
